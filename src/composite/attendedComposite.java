/**
 * 
 */
package composite;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ANDRI
 *
 */
public class attendedComposite implements attended  {

	private List<attended> attendedList = new ArrayList<attended>();
	
	@Override
	public void displayAttended() {
		for(attended att:attendedList) {
			att.displayAttended();
		}	
	}
	
	public void addAttended(attended att) {
		attendedList.add(att);
	}
	
	public void removeAttended(attended att) {
		attendedList.remove(att);
	}

}
