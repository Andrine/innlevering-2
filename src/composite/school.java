package composite;

public class school {

	public static void main(String[] args) {
		
		teacher teach1 = new teacher(211060, "Missy Flatby", "Teacher", "NTNU Trondheimd", "Mattematikk 3");
		teacher teach2 = new teacher(191161, "Patrick Nordbo", "Teacher", "NTNU Gj�vik", "Mattematikk 1");		
		attendedComposite teacherAttended = new attendedComposite();
		teacherAttended.addAttended(teach1);
		teacherAttended.addAttended(teach2);

		student stund1 = new student(131097, "Andrine Celine Flatby", "Student", "NTNU Trondheim", "Mattematikk 3");
		student stund2 = new student(140697, "Ida Harbak", "Student", "NTNU Gj�vik", "Mattematikk 1");		
		attendedComposite studentAttended = new attendedComposite();
		studentAttended.addAttended(stund1);
		studentAttended.addAttended(stund2);
		
		attendedComposite attendent = new attendedComposite();
		attendent.addAttended(teacherAttended);
		attendent.addAttended(studentAttended);
		attendent.displayAttended();
	}

}
