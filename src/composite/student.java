/**
 * 
 */
package composite;

/**
 * @author ANDRI
 *
 */
public class student implements attended {
	
	private String status;
	private String name;
	private int id;
	private String institute;
	private String subject;

	
	public student (int id, String name, String status, String institute, String subject) {
		this.id= id;
		this.name= name;
		this.status= status;
		this.institute= institute;
		this.subject = subject;
	}
	@Override
	public void displayAttended() {
		System.out.println(id+" "+name+" "+status+" "+institute+" "+ subject);
		
	}

}
